class CreateRefugees < ActiveRecord::Migration
  def change
    create_table :refugees do |t|
      t.string :last_name
      t.string :first_name
      t.string :country
      t.string :city
      t.text :custom_field

      t.timestamps
    end
  end
end
