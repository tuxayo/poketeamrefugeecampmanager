class AddCampIdToRefugees < ActiveRecord::Migration
  def change
    add_reference :refugees, :camp, index: true
  end
end
