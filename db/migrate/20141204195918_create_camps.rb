class CreateCamps < ActiveRecord::Migration
  def change
    create_table :camps do |t|
      t.string :name
      t.string :country
      t.string :city
      t.integer :capacity
      t.float :coordinate_x
      t.float :coordinate_y
      t.text :custom_field

      t.timestamps
    end
  end
end
