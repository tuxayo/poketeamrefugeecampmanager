require 'capybara'
require 'capybara/poltergeist'
require 'capybara/rspec'

include Capybara::DSL
include RSpec::Matchers

Capybara.register_driver :poltergeist do |app|
  Capybara::Poltergeist::Driver.new(app, debug: false)
end

Capybara.configure do |config|
  config.run_server = false
  config.app_host = 'http://localhost:3000'
  config.default_driver = :poltergeist
end


visit('/admin/refugees')

page.should have_content('Pika');
puts "test passed"