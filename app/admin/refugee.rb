ActiveAdmin.register Refugee do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  permit_params :last_name, :first_name, :country, :city, :custom_field, :camp_id
  filter :camp
  filter :last_name
  filter :first_name
  filter :country
  filter :city

  index do
    column :last_name
    column :first_name
    column :country
    column :city
    column :camp
    actions
  end
end
