ActiveAdmin.register Camp do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :name, :country, :city, :capacity, :coordinate_x, :coordinate_y, :custom_field
  filter :name
  filter :country
  filter :city
  filter :capacity
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  form do |f|
    f.inputs do
      f.input :name
      f.input :country, :priority_countries => []
      f.input :city
      f.input :capacity
      f.input :coordinate_x
      f.input :coordinate_y
      f.input :custom_field
    end
    f.actions
  end

  index do
    column :name
    column :country
    column :city
    column :capacity
    actions
  end
end
