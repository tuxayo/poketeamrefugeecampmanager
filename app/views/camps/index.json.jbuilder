json.array!(@camps) do |camp|
  json.extract! camp, :id, :name, :country, :city, :capacity, :custom_field
  json.url camp_url(camp, format: :json)
end
